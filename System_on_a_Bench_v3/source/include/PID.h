/*
 * PID.h
 *
 *  Created on: Apr 18, 2022
 *      Author: artur
 */

#ifndef INCLUDE_PID_H_
#define INCLUDE_PID_H_

#include "components.h"

#define TICKS_PER_ROT 819
#define SATURATION TICKS_PER_ROT
#define TS 50 // Time step in milliseconds
#define P 1.8 // Proportional gain
#define I 0.06 // Integral gain
#define D 0 // Derivative gain

void PID_init(uint16_t currentPos);

void PID_setPosition(uint16_t command);

int8_t PID_update(uint16_t curPos);

int8_t PID_getControlEffort(void);

#endif /* INCLUDE_PID_H_ */
