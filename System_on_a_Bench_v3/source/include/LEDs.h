/*
 * LEDs.h
 *
 *  Created on: Mar 5, 2022
 *      Author: svenka12
 */

#ifndef LEDS_H_
#define LEDS_H_

#include "components.h"

#define OFF_LED_PORT		PORT_F
#define OFF_LED_PIN 		PIN_GPIO80

#define LAPTOP_LED_PORT 	PORT_D
#define LAPTOP_LED_PIN 		PIN_GPIO57

#define GAMEPAD_LED_PORT	PORT_D
#define GAMEPAD_LED_PIN		PIN_GPIO58

#define OFF_BUTTON_PORT		PORT_B
#define OFF_BUTTON_PIN		PIN_GPIO27

#define LAPTOP_BUTTON_PORT	PORT_B
#define LAPTOP_BUTTON_PIN	PIN_GPIO26

#define GAMEPAD_BUTTON_PORT	PORT_B
#define GAMEPAD_BUTTON_PIN	PIN_GPIO25





void system_off_LED(void) {
	pal_lld_setpad(OFF_LED_PORT, OFF_LED_PIN);
	pal_lld_clearpad(LAPTOP_LED_PORT, LAPTOP_LED_PIN);
	pal_lld_clearpad(GAMEPAD_LED_PORT, GAMEPAD_LED_PIN);
}


void gamepad_LED(void) {
	pal_lld_clearpad(OFF_LED_PORT, OFF_LED_PIN);
	pal_lld_clearpad(LAPTOP_LED_PORT, LAPTOP_LED_PIN);
	pal_lld_setpad(GAMEPAD_LED_PORT, GAMEPAD_LED_PIN);

}
void laptop_LED(void) {
	pal_lld_clearpad(OFF_LED_PORT, OFF_LED_PIN);
	pal_lld_setpad(LAPTOP_LED_PORT, LAPTOP_LED_PIN);
	pal_lld_clearpad(GAMEPAD_LED_PORT, GAMEPAD_LED_PIN);
}


#endif /* LEDS_H_ */


