#include <stdint.h>

#include "main.h"


#define CHECKSUM_ERROR_LENGTH 100

typedef enum{
	//error codes sent from the central controller
	FINE = 0, 		//system is fine, no error
	RELEASECONFIRM = 99 //System has successfully released DBW control
	//ADD MORE
} ErrorCodeCC;

typedef enum {
	//error codes recieved from the top-level
	//FINE = 0, 		//system is fine, no error
	RELEASE = 99,	//release system from DBW system, user controls everything now
} ErrorCodeTL;

//void cmd2packet(uint8_t *packet, CarValues cmd, uint8_t errorCode, int checksum){
//    packet[0] = (cmd.steering_angle >> 8) & 0xFF;
//    packet[1] = (cmd.steering_angle) & 0xFF;
//    packet[2] = (cmd.percent_brake) & 0xFF;
//    packet[3] = (cmd.percent_throttle) & 0xFF;
//    packet[4] = packet[4] | (cmd.horn) & 0x01;
//    packet[4] = packet[4] | (cmd.reverse << 1) & 0x02;
//    packet[4] = packet[4] | (cmd.windshield_wiper << 2) & 0x04;
//    packet[4] = packet[4] | (cmd.E_STOP << 3) & 0x08;
//    packet[4] = packet[4] | (cmd.left_turn << 4) & 0x10;
//    packet[4] = packet[4] | (cmd.right_turn << 5) & 0x20;
//    packet[5] = errorCode & 0xFF;
//    //packet[6] = checksum >> 24 & 0xFF;
//    //packet[7] = checksum >> 16 & 0xFF;
//    //packet[8] = checksum >> 8 & 0xFF;
//    //packet[9] = checksum & 0xFF;
//}

//uint8_t packet2values(uint8_t *packet, CarValues *curValues){
//    curValues->steeringAngle = (packet[0] << 8) + packet[1];
//    curValues->percentBrake = packet[2];
//    curValues->percentThrottle = packet[3];
//    curValues->horn = packet[4] & 0x01;
//    curValues->reverse = (packet[4] & 0x02) >> 1;
//    curValues->headlights = (packet[4] & 0x04) >> 2;
//    curValues->ESTOP = (packet[4] & 0x08) >> 3;
//    curValues->leftTurn = (packet[4] & 0x10) >> 4;
//    curValues->rightTurn = (packet[4] & 0x20) >> 5;
//    return packet[5]; //returns the error code
//}


uint8_t packet2values(uint8_t *packet, CarValues *curValues){
    curValues->steeringAngle = (packet[0] << 8) + packet[1];
    curValues->percentBrake = packet[2];
    curValues->percentThrottle = packet[3];
    curValues->horn = packet[4] & 0x01;
    curValues->reverse = (packet[4] & 0x02) >> 1;
    curValues->headlights = (packet[4] & 0x04) >> 2;
    curValues->ESTOP = (packet[4] & 0x08) >> 3;
    curValues->leftTurn = (packet[4] & 0x10) >> 4;
    curValues->rightTurn = (packet[4] & 0x20) >> 5;
    uint8_t errorCode = packet[5];

    unsigned long givenCS = (packet[6] << 24) + (packet[7] << 16) + (packet[8] << 8) + packet[9];
    char buffer[6*sizeof(uint8_t)];
    sprintf(buffer, "%u%u%u%u%u%u", packet[0], packet[1], packet[2], packet[3], packet[4], packet[5]);
    unsigned long calcCS = crc32(0, buffer, 6*sizeof(uint8_t));
    if (givenCS != calcCS){
        errorCode += 100; //any error code over 100 indicates checksum mismatch + regular error(0-99)
    }
    return errorCode; //returns the error code
}

