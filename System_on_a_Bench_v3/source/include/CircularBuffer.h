/*
 * serial.h
 *
 *  Created on: Apr 21, 2022
 *      Author: artur
 */

#ifndef INCLUDE_CIRCULARBUFFER_H_
#define INCLUDE_CIRCULARBUFFER_H_

#include "components.h"

#define CIRCULAR_LEN 6

void circular_init(void);

uint16_t circular_append(uint8_t *buf, uint16_t len);

uint16_t circular_getLen(void);

uint16_t circular_getBuf(uint8_t *buf, uint16_t len);

void circular_clear(void);


#endif /* INCLUDE_CIRCULARBUFFER_H_ */
