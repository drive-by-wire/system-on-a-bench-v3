/*
 * main.h
 *
 *  Created on: Apr 13, 2022
 *      Author: artur
 */

#ifndef MAIN_H_
#define MAIN_H_

/*
*  Authors: Arturo Gamboa Gonzales, Sutter Lum, Sriram Venkataraman
*  Description: Header file for the HSL Drive-by-Wire Senior Design Project
*/

/* ============================================================================
 *                           DEFINES SECTION
 * ==========================================================================*/

#define RX_LEN 20

/* ============================================================================
 *                           ENUMS SECTION
 * ==========================================================================*/

typedef enum {
    START,      // Vehicle stays here until a switch is pressed (indicated by input_mode)
    SETUP,      // Perform necessary actions to put vehicle in OPERATION state
    OPERATION,  // The vehicle will remain in this state under nominal conditions
    ERROR       // The system will remain idle in this state until the reset is pressed
} CarState;

typedef enum {
    SYSTEMOFF, GAMEPAD, LAPTOP
} InputMode;

#define FAILURE 0
#define SUCCESS 1

#define False 0
#define True 1

/* ============================================================================
 *                        CLASS DEFINITION SECTION
 * ==========================================================================*/

typedef struct {
        double steeringAngle;
        double percentBrake;
        double percentThrottle;
        bool horn;
        bool reverse;
        bool headlights;
        bool ESTOP;
        bool leftTurn;
        bool rightTurn;

} CarValues;


CarValues* car_values_init(void) {
    CarValues* newCarValues  = malloc(sizeof(CarValues));
    newCarValues->steeringAngle = 0;
    newCarValues->percentBrake = 0;
    newCarValues->percentThrottle = 0;
    newCarValues->horn = False;
    newCarValues->reverse = False;
    newCarValues->headlights = False;
    newCarValues->ESTOP = False;
    newCarValues->leftTurn = False;
    newCarValues->rightTurn = False;
    return newCarValues;
}

void car_values_reset(CarValues *curr) {
    curr->steeringAngle = 0;
    curr->percentBrake = 0;
    curr->percentThrottle = 0;
    curr->horn = False;
    curr->reverse = False;
    curr->headlights = False;
    curr->ESTOP = False;
    curr->leftTurn = False;
    curr->rightTurn = False;
}

void car_values_overwrite(CarValues *prev, CarValues *curr) {
    prev->steeringAngle = curr->steeringAngle;
    prev->percentBrake = curr->percentBrake;
    prev->percentThrottle = curr->percentThrottle;
    prev->horn = curr->horn;
    prev->reverse = curr->reverse;
    prev->headlights = curr->headlights;
    prev->ESTOP = curr->ESTOP;
    prev->leftTurn = curr->leftTurn;
    prev->rightTurn = curr->rightTurn;
}


#endif /* MAIN_H_ */

