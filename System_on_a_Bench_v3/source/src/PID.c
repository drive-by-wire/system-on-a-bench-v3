/*
 * PID.c
 *
 *  Created on: Apr 18, 2022
 *      Author: artur
 */

#include <stdlib.h>
#include "components.h"
#include "PID.h"


static float a1; // Previous accumulator
static int8_t control; // Current control effort
static uint16_t x1; // Previous position
static uint16_t cmdPos;


void PID_init(uint16_t currentPos){
	a1 = 0;
	control = 0;
	x1 = currentPos;
	cmdPos = currentPos;
}

void PID_setPosition(uint16_t command){
	cmdPos = command;
}

int8_t PID_update(uint16_t curPos){
	float u_p; // Proportional controller
	float u_i; // Integral controller
	float u_d; // Derivative Controller
	float a0; // Current accumulator
	float u0;
	uint16_t err = cmdPos - curPos;
	u_p = P * (float)err;
	u_d = D * (float)(x1 - curPos) * 1000 / TS;
	a0 = a1 + (float)err * 0.05;
	u_i = I * a0;
	u0 = u_p + u_i + u_d;
	if(abs(u0) > SATURATION){
	  a0 = a1;
	  u_i = 0;
	  u0 = (u0 > 0)? TICKS_PER_ROT : -TICKS_PER_ROT;
	}

	a1 = a0;
	x1 = curPos;
	control = (int8_t)(u0 * 100 / TICKS_PER_ROT);

	return control;
}

int8_t PID_getControlEffort(void){
	return control;
}

