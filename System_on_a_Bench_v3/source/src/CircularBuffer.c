/*
 * serial.c
 *
 *  Created on: Apr 21, 2022
 *      Author: artur
 */


#include <stdint.h>
#include <stdio.h>
#include "CircularBuffer.h"

static uint8_t buffer[CIRCULAR_LEN];
static uint16_t readPtr;
static uint16_t writePtr;

void circular_init(void){
	uint16_t i;
	readPtr = 0;
	writePtr = 0;
	for(i = 0; i < CIRCULAR_LEN; i++)
		buffer[i] = 0;
}

uint16_t circular_append(uint8_t *buf, uint16_t len){
	uint16_t i;
	for(i = 0; i < len; i++){
		buffer[writePtr] = buf[i];
		writePtr++;
		if(writePtr >= CIRCULAR_LEN)
			writePtr = 0;
        if(writePtr == readPtr){
            readPtr++;
            if(readPtr >= CIRCULAR_LEN)
                readPtr = 0;
        }
	}
	//if(writePtr >= readPtr && writePtr != CIRCULAR_LEN - 1)
	//	readPtr++;
	//if(writePtr >= readPtr && writePtr != CIRCULAR_LEN - 1){
    //    readPtr = writePtr +1;
	//}
	//else if(writePtr == CIRCULAR_LEN - 1)
    //    readPtr = 0;
	//return (len > CIRCULAR_LEN)? CIRCULAR_LEN : len;
}

uint16_t circular_getLen(void){
	uint16_t len, i;
	if(writePtr >= readPtr)
		return writePtr - readPtr;
	for(i = readPtr, len = 0; len < CIRCULAR_LEN; len++){
		if(i == writePtr)
			return len;
		i++;
		if(i >= CIRCULAR_LEN)
			i = 0;
	}
	return len;
}

uint16_t circular_getBuf(uint8_t *buf, uint16_t len){
	uint16_t i, j;
	for(i = 0, j = readPtr; i < len; i++){
		buf[i] = buffer[j];
		j++;
		if(j >= CIRCULAR_LEN)
			j = 0;
	}
	return (i > CIRCULAR_LEN)? CIRCULAR_LEN : i;
}

void circular_clear(void){
	circular_init();
}


//test written by Sutter Lum, 5/2/2022
//#define TEST //uncomment to test
#ifdef TEST
    int main(void){
	    circular_init();
        uint8_t output[10];
        uint8_t input[10];
        input[0] = 1;
        input[1] = 2;
        input[2] = 3;
        input[3] = 4;
        input[4] = 5;
        input[5] = 6;
        input[6] = 7;
        input[7] = 8;
        input[8] = 9;
        uint16_t len = 0;

        circular_append(input, 8);

        circular_getBuf(output, 6); //should read 4, 5, 6, 7, 8

        printf("output is %d, %d, %d, %d, %d\n", output[0], output[1], output[2], output[3], output[4]);
        len = circular_getLen();
        printf("length is: %d\n", len);
        return 1;
    }

#endif
