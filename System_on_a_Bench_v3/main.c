/****************************************************************************
 *
 * Authors: Arturo Gamboa Gonzales, Sutter Lum, Sriram Venkataraman
 *
 * Description: Source file for the HSL Drive-by-Wire Senior Design Project
 *
 *****************************************************************************/

/* ============================================================================
 *                           INCLUDE SECTION
 * ==========================================================================*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include "components.h"
#include "pit_lld_cfg.h"
#include "saradc_lld_cfg.h"
#include "pwm_lld_cfg.h"
#include "spi_lld_cfg.h"
#include "serial_lld_cfg.h"
#include "main.h"
#include "LEDs.h"
#include "PID.h"
#include "peripherals.h"
#include "packet_builder.h"
//#include "FreeRTOS.h"
//#include "task.h"

#define ADC59 59U // DAC CHECK
#define ADC60 60U
#define ADC62 62U
#define ADC63 63U

#define TICKS_PER_ROT 819 // Number of ADC ticks per rotation of the steering column
#define TICK_PERIOD 100 //10 kHz for PWM
#define DELAY 0 // PWM Delay
#define TRIGGER 0 // PWM Trigger
#define CH0 0 // PWM Channel 0
#define CH1 1 // PWM Channel 1
#define SPI_LEN 2 // SPI Buffer Length
#define CLEAR_BUF(buf, len) for(uint16_t i_macro; i_macro < len; i_macro++) buf[i_macro] = 0;
#define PACKET_LEN 6 // Length of the package to be received. THIS NEEDS TO BE CHANGED
#define JOYSTICK_SERIAL SD1 // Serial channel for the joystick
#define JETSON_SERIAL SD2 // Serial channel for the jetson
#define ACCEL_OFF 245

/* ============================================================================
 *                       PRIVATE VARIABLES SECTION
 * ==========================================================================*/

// Flags //
static vuint8_t pitFlag; // Timer flag
static vuint8_t isDataReady; // ADC flag

// ADC variables //
static vuint16_t accelCheck;
static vuint16_t colADC; // ADC channel 63
static vuint16_t brakeADC1; // ADC channel 60
static vuint16_t brakeADC2; // ADC channel 62

// Operation state variables //
static volatile InputMode inputMode = SYSTEMOFF;
static volatile bool inMotion = False;
static volatile bool resetWasPressed = False;
static volatile bool systemOFF = False;
static CarState currentState = START;

// Buffers //
static uint8_t joystickBuf[RX_LEN]; // Serial joystick receive buffer
static uint8_t joystickIndex; // Current index of the joystick receive buffer
static uint8_t jetsonBuf[RX_LEN]; // Serial joystick receive buffer
static uint8_t jetsonIndex; // Current index of the joystick receive buffer
static uint8_t spiBuf[SPI_LEN]; // SPI transmit Buffer

/* ============================================================================
 *                       PRIVATE FUNCTIONS SECTION
 * ==========================================================================*/

	/*
 * Description: Creates the SPI message for the MCP4812 DAC
 * Arguments:
 *   buf - buffer to be written with the message (min len 2)
 *   level - the base 1024 voltage level
 * 
 * Return:
 *   None
 */
void dac_message(uint8_t *buf, uint16_t level){
	level = level & 0x3FF;
	buf[0] = 0x10 + (level >> 6);
	buf[1] = (level << 2) & 0xFF;
}


/*
 * Description: Commands the accelerator using a DAC
 * Arguments:
 *   current - container of the new desired values
 *   prev - container of the previous desired values
 *
 * Return:
 *   SUCCESS if DAC is connected, FAILURE if disconnnected
 */
void update_throttle(CarValues *current) {
	dac_message(spiBuf, current->percentThrottle * 10); // THIS VALUE IS INCORRECT. TODO: CALCULATE SCALING
	spi_lld_send(&SPID1, 2, spiBuf);
}

/*
 * Description: Commands the brake actuator to the desired position
 * Arguments:
 *   current - container of the new desired values
 *   prev - container of the previous desired values
 *
 * Return:
 *  SUCCESS if actuator is connected, FAILURE if disconnnected
 */
bool update_braking(CarValues *current, CarValues *prev) {
	(void)current;
	(void)prev;
	// print('setting brake')
	return SUCCESS;
}


/*
 * Description: Commands the steering actuator to the desired position
 * Arguments:
 *   current - container of the new desired values
 *   prev - container of the previous desired values
 * Return:
 *   SUCCESS if actuator is connected, FAILURE if disconnnected
 */
bool update_steering(CarValues *current, CarValues *prev) {
	(void)current;
	(void)prev;
	// print('setting steering')
	return SUCCESS;
}

/*
 * Description: Controlling the peripherals
 * Arguments:
 *   current - container of the new desired values
 *
 * Return:
 *  NULL
 */
void update_peripherals(CarValues *current, CarValues *prev) {

	// toggle left turn light on button down event
	if (current->leftTurn == True && prev->leftTurn == False) pal_lld_togglepad(LEFT_TURN_LED_PORT, LEFT_TURN_LED_PIN);

	// toggle right turn light on button down event
	if (current->rightTurn == True && prev->rightTurn == False) pal_lld_togglepad(RIGHT_TURN_LED_PORT, RIGHT_TURN_LED_PIN);

	// Set GPIO for horn
	if (current->horn == True) pal_lld_setpad(HORN_PORT, HORN_PIN);
	else pal_lld_clearpad(HORN_PORT, HORN_PIN);

	// toggle headlights on button down event
	if (current->headlights == True && prev->headlights == False) pal_lld_togglepad(HEADLIGHT_PORT, HEADLIGHT_PIN);

	return;
}


/*
 * Description: Verify that the brake actuator & brake angle sensor are connected
 * Arguments:
 *   NULL
 *
 * Return:
 *  NULL
 */
bool brake_test(void) {
	//display(starting braking actuator test)
	//move linear actuator to 50% extension
	// if brake angle isn't ~50%, return False
	//move linear actuator to 100% extension
	// if brake angle isn't ~100%, return False
	//return linear actuator to default position
	return SUCCESS;
}


/*
 * Description: Verify that the steering actuator & steering angle sensor are connected
 * Arguments:
 *   NULL
 *
 * Return:
 *  NULL
 */
bool steering_test(void) {
	// display (starting steering actuator test)
	// move steering angle to 5 degrees
	// if steering angle error is >10%, return False
	// move steering angle to -5 degrees
	// is steering angle error is >10%, return False
	// return steering angle to default position
	return SUCCESS;
}


/*
 * Description: Verify that the DAC isn't fried
 * Arguments:
 *   NULL
 *
 * Return:
 *  NULL
 */
bool DAC_test(void) {
	//command a voltage on the DAC
	//if the voltage read from ADC varies by 10%, return FALSE
	dac_message(spiBuf, ACCEL_OFF);
	spi_lld_send(&SPID1, 2, spiBuf);
	osalThreadDelayMilliseconds(10);
	isDataReady = 0;
	while(!isDataReady);
	accelCheck = accelCheck >> 2;
	if(accelCheck < ACCEL_OFF - 24 || accelCheck > ACCEL_OFF + 24) return FAILURE;
	return SUCCESS;
}



int main(void) {
	
	// START FEEDBACK VARS //
	int8_t dutyCycle; // The duty cycle of the h-bridge motor driver
	uint32_t curPos; // The current steering column position
	int32_t err; // 
	float u_p,u_i, u0, a0, a1;
	// END FEEDBACK VARS//
	uint32_t cmdPos = 2048; // Value to be passed to PID functions
	isDataReady = 0; // initializing ADC Flag

	// Initialize ADC values
	accelCheck = 0;
	colADC = 0; // ADC adcVal
	brakeADC1 = 0;
	brakeADC2 = 0;

	joystickIndex = 0;
	jetsonIndex = 0;
	pitFlag = FALSE;

	// Initialize buffers to zero
	CLEAR_BUF(spiBuf, SPI_LEN);
	CLEAR_BUF(joystickBuf, RX_LEN);
	CLEAR_BUF(jetsonBuf, RX_LEN);

	/* Initialization of all the imported components in the order specified in
	 the application wizard. The function is generated automatically.*/
	componentsInit();

	/* Enable Interrupts */
	irqIsrEnable();

	// create the containers for values
	CarValues* newVals = car_values_init();
	CarValues* prevVals = car_values_init();

	// GAMEPAD init (serial)
	sd_lld_start(&JOYSTICK_SERIAL, &serial_config_packet_cfg);
	sd_lld_start(&JETSON_SERIAL, &serial_config_packet_cfg);

	// initialize LEDs
	system_off_LED();

	// timer to read input buttons
	pit_lld_start(&PITD1, pit0_config);
	pit_lld_channel_start(&PITD1,PIT0_CHANNEL_CH1);

	// Enable PWM Group
	pwm_lld_init();
	pwm_lld_start(&PWMD9, &pwm_config_h_bridge);

	// Enable SARADC
	saradc_lld_start(&SARADC12DSV, &saradc_config_adcconfig);
	saradc_lld_start_conversion(&SARADC12DSV);

	while(!isDataReady);
	isDataReady = FALSE;

	printf("ADC: %d\r\n", colADC);
	PID_init(colADC);

	PID_setPosition(2000);

	// DAC initialization
	spi_lld_start(&SPID1, &spi_config_mcp4812);


	while (1) {

// TO DO: ADD FUNCTION TO DO ALL SERIAL READS

		// Check rx buffer
		while((int)JOYSTICK_SERIAL.rx_write_ptr != (int)JOYSTICK_SERIAL.rx_read_ptr && joystickIndex < PACKET_LEN)
			joystickIndex += sd_lld_read(&JOYSTICK_SERIAL, joystickBuf + joystickIndex, 1);

		while((int)JETSON_SERIAL.rx_write_ptr != (int)JETSON_SERIAL.rx_read_ptr && jetsonIndex < PACKET_LEN)
					jetsonIndex += sd_lld_read(&JETSON_SERIAL, jetsonBuf + jetsonIndex, 1);

		//add serial read from higher level controller

		switch (currentState) {

		/**********************************************************************************
		* START State: Vehicle stays here until a switch is pressed (indicated by inputMode)
		***********************************************************************************/
			case (START):

			if (inputMode == GAMEPAD || inputMode == LAPTOP) {

				if (inMotion == True) {
					currentState = ERROR;
					system_off_LED();
				} //don't enable the system if the car is in motion

				else currentState = SETUP;

			}
			break;

		/**********************************************************************************
		* SETUP State: Perform necessary actions to put vehicle in OPERATION state
		***********************************************************************************/
			case (SETUP):

			//verify that the mode select E-STOP is working
			// display ("Press the systemOFF button!");
//			while (systemOFF == False);

			if ( (brake_test() == SUCCESS) && (steering_test() == SUCCESS) && (DAC_test() == SUCCESS) ) {
				currentState = OPERATION;
				(inputMode == GAMEPAD) ? gamepad_LED() : laptop_LED();
			}
			else {
				currentState = ERROR;
				system_off_LED();
			}
			break;

		/**********************************************************************************
		* OPERATION State: The vehicle will remain in this state under nominal conditions
		***********************************************************************************/
			case (OPERATION):

			if (inputMode == SYSTEMOFF) continue;

			if (systemOFF == True) {
				currentState = ERROR;
				system_off_LED();
				break;
			} // see if mode select E-STOP has been pressed

			//TO DO: CHECKSUM STUFF

			// check if the laptop is connected, and parse the values
			if (inputMode == LAPTOP && jetsonIndex == PACKET_LEN) {
				if (packet2values(jetsonBuf, newVals) > CHECKSUM_ERROR_LENGTH) {
					currentState = ERROR;
					system_off_LED();
					break;
				}
				CLEAR_BUF(jetsonBuf, 6);
				jetsonIndex = 0;
				CLEAR_BUF(joystickBuf, RX_LEN);
				joystickIndex = 0;

			}

			// check if the gamepad is connected, and parse the values
			if (inputMode == GAMEPAD && joystickIndex == PACKET_LEN) {
				if (packet2values(joystickBuf, newVals) > CHECKSUM_ERROR_LENGTH) {
					currentState = ERROR;
					system_off_LED();
				}
				CLEAR_BUF(joystickBuf, 6);
				joystickIndex = 0;
//				cmdPos = 1000 + (newVals->steeringAngle) * 2 / 5;
				cmdPos = newVals->steeringAngle; //this mapping happens on the joystick side

			}

			if ( (newVals->ESTOP) == True) {
				currentState = ERROR;
				system_off_LED();
				break;
			}

			// Updating the DAC with the desired voltage
			// TO DO: add check with ADC?
			update_throttle(newVals);

			// Updating the extension of the brake actuator
			// TO DO: add check to see if connected
			if (update_braking(newVals, prevVals) == FAILURE) {
				currentState = ERROR;
				system_off_LED();
				break;
			}

			// Updating the extension of the steering actuator
			// TO DO: add check to see if connected
			if (update_steering(newVals, prevVals) == FAILURE) {
				currentState = ERROR;
				system_off_LED();
				break;
			}

			update_peripherals(newVals, prevVals);

			car_values_overwrite(prevVals, newVals);

			break;

		/**********************************************************************************
		* ERROR State: The system will remain idle in this state until the reset is pressed
		***********************************************************************************/
			case (ERROR):
			if (resetWasPressed == True) {
				currentState = START;
				inputMode = SYSTEMOFF;
				resetWasPressed = False;
				car_values_reset(newVals);
				car_values_reset(prevVals);
			}
			break;

		/**********************************************************************************
		* Update the display with pertinent information
		***********************************************************************************/
		//figure out i2c coms for display

		}

		if(pitFlag){
			while(!isDataReady);
//			control = PID_update(value);
//
//			printf("ADC: %d\r\n", control);
//
//			if(control > 0){
//				pwm_lld_enable_channel(&PWMD9, CH1, TICK_PERIOD, 0, DELAY, TRIGGER);
//				pwm_lld_enable_channel(&PWMD9, CH0, TICK_PERIOD, control, DELAY, TRIGGER);
//			}
//			else{
//				pwm_lld_enable_channel(&PWMD9, CH0, TICK_PERIOD, 0, DELAY, TRIGGER);
//				pwm_lld_enable_channel(&PWMD9, CH1, TICK_PERIOD, abs(control), DELAY, TRIGGER);
//			}

			curPos = colADC;
			err = cmdPos - curPos;
			u_p = 1.8 * (float)err;
			a0 = a1 + (float)err * 0.05;
			u_i = (0.06 * a0);
			u0 = u_p + u_i;
			if(abs(u0) > TICKS_PER_ROT){
				a0 = a1;
				u_i = 0;
				u0 = (u0 > 0)? TICKS_PER_ROT : -TICKS_PER_ROT;
			}

			dutyCycle = (uint8_t) 100 * abs(u0) / TICKS_PER_ROT;

		  // printf("ADC: %ld, cmd: %ld, u0: %f\r\n", curPos, cmdPos, u0);

			if(u0 > 0){
				pwm_lld_enable_channel(&PWMD9, CH1, TICK_PERIOD, 0, DELAY, TRIGGER);
				pwm_lld_enable_channel(&PWMD9, CH0, TICK_PERIOD, dutyCycle, DELAY, TRIGGER);
			}
			else{
				pwm_lld_enable_channel(&PWMD9, CH0, TICK_PERIOD, 0, DELAY, TRIGGER);
				pwm_lld_enable_channel(&PWMD9, CH1, TICK_PERIOD, dutyCycle, DELAY, TRIGGER);
			}


			a1 = a0;

			pitFlag = FALSE;
			isDataReady = FALSE;
		}
	}

}

/* ============================================================================
 *                           ISR FUNCTIONS
 * ==========================================================================*/

/*
 * Description: Checks buttons on the mode select panel
 * Arguments:
 *  void
 *
 * Return:
 *  void
 */
void pit_isr(void){
	pitFlag = TRUE;

	int gamepadTriggered = pal_lld_readpad(GAMEPAD_BUTTON_PORT, GAMEPAD_BUTTON_PIN);

	int laptopTriggered = pal_lld_readpad(LAPTOP_BUTTON_PORT, LAPTOP_BUTTON_PIN);

	if (pal_lld_readpad(OFF_BUTTON_PORT, OFF_BUTTON_PIN) == 1) systemOFF = True;

//	if (pal_lld_readpad(RESET_BUTTON_PORT, RESET_BUTTON_PIN) == 1) resetWasPressed = True;

	if (gamepadTriggered && laptopTriggered) return;
	else if (gamepadTriggered || laptopTriggered) inputMode = gamepadTriggered ? GAMEPAD : LAPTOP;

}


/*
 * Description: Reads the adc value and sets a flag
 *              saying there is new data
 * Arguments:
 *  saradcp - the SARADC Driver
 *
 * Return:
 *  void
 */
void adcconfig_adc_isr(SARADCDriver *saradcp){
	accelCheck = saradc_lld_readchannel(saradcp, ADC59);
	colADC = saradc_lld_readchannel(saradcp, ADC63);
	brakeADC1 = saradc_lld_readchannel(saradcp, ADC60);
	brakeADC2 = saradc_lld_readchannel(saradcp, ADC62);
	isDataReady = 1;
}
